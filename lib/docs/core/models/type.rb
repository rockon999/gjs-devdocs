module Docs
  Type = Struct.new :name, :count, :default do
    attr_accessor :slug

    def initialize(*args)
      super
      self.count ||= 0
    end

    def slug
      name.parameterize
    end

    def default_entry
      default
    end

    def as_json
      to_h.merge! slug: slug
    end
  end
end
