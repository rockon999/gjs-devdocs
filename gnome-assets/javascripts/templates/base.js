app.templates.render = function(name, value, ...args) {
  const template = app.templates[name];

  if (Array.isArray(value)) {
    let result = '';
    for (let val of value) {
      // Array.from
      result += template(val, ...Array.from(args)); // TODO Usage of Array.from
    }
    return result;
  } else if (typeof template === 'function') {
    return template(value, ...Array.from(args)); // TODO Usage of Array.from
  } else {
    return template;
  }
};
